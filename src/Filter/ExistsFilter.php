<?php

namespace Ercan\ElasticQueryBuilder\Filter;

class ExistsFilter extends Filter
{
    protected $field;

    public function setField(string $field)
    {
        $this->field = $field;

        return $this;
    }

    public function build(): array
    {
        return [
            'exists' => [
                'field' => $this->field,
            ],
        ];
    }
}